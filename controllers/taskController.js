const Task = require('../models/task');


//Controller Function for getting all the tasks

module.exports.getAllTasks = () => {
	return Task.find({}).then( result => {
		return result;
	})
};




//Creating a task
module.exports.createTask = (requestBody) => {

	//Create object
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})

}


//Deleting a task
//"id" url parameter passed from the taskRoute.js

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}


//Update a task

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} 

		result.name = newContent.name; 

		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})

	})
}

//Get a specific task

module.exports.getSpecificTasks = (taskId) => {
	return Task.findById(taskId).then( result => {
		return result;
	})
}

// Updating status of a task

module.exports.changeStatusTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} 

		result.status = "complete"; 

		return result.save().then((changeStatusTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return changeStatusTask;
			}
		})

	})
}



















